import Controlador.c_productos as c_productos
import Controlador.c_clientes as c_cliente
import Controlador.c_proveedores as c_proveedores
import Controlador.c_compras as c_compras
import Controlador.c_ventas as c_ventas

def menu():
    list_menu = ["*** BIENVENIDO AL MENU GENERA ***","a) Productos","b) Clientes","c) Proveedores","d) Compras","e) Ventas","x) Salir"]
    for a in list_menu:
        print(a)
volver = "si"
while volver == "si":
    menu()
    opcion = input("Ingrese una opción : ").lower()
    if opcion == "a":
        c_productos.ejecutar()
    elif opcion == "b":
        c_cliente.ejecutar()
    elif opcion == "c":
        c_proveedores.ejecutar()
    elif opcion == "d":
        c_compras.ejecutar()
    elif opcion == "e":
        c_ventas.ejecutar()
    elif opcion == "x":
        print("ADIÓS")
        break
    else: 
        print("Ingrese a/b/c/d/x")