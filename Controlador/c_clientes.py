from Modelo.Clientes import *
from Modelo.AccesoDatos.Datos import *

def menu():
    list_menu = ["*** BIENVENIDO AL MENU CLIENTES ***","a) Registrar","b) Listar","c) Editar","d) Eliminar","x) Salir"]
    for a in list_menu:
        print(a)

def ejecutar():
    volver = "si"
    while volver == "si":
        menu()
        opcion = input("Elija una opción: ").lower()
        if opcion == "a":
            registrar()
        elif opcion == "b":
            listar()
        elif opcion == "c":
            editar()
        elif opcion == "d":
            eliminar()
        elif opcion == "x":
            volver_productos = input("Desea volver al Menú General ? : ").lower()
            if volver_productos == "si":
                volver = "no"
        else: 
            print("Ingrese a/b/c/d/x")

def registrar():
    nombre = input("Nombre: ").capitalize()
    apellido = input("Apellido: ").capitalize()
    dni = input("DNI: ")
    obj_cliente = Clientes(nombre,apellido,dni)
    list_clientes.append(obj_cliente)
def listar():
    for item, dato in enumerate(list_clientes, 1):
        if item == 1:
            print("Item","Nombre","Apellido","Dni",sep= "   |   ")
        print(item,"   |   " ,dato.nombre,"   |   ",dato.apellido,"   |   ", dato.dni,)
def editar():
    listar()
    fila = int(input("Elija un cliente: "))
    fila -= 1
    nombre = input('Nuevo nombre: ')
    apellido = input('Nuevo apellido: ')
    dni = input('Nuevo dni: ')

    list_clientes[fila].nombre = nombre
    list_clientes[fila].apellido = apellido
    list_clientes[fila].dni = dni

def eliminar():
    listar()
    fila = int(input("Elija un cliente: "))
    fila -= 1
    del list_clientes[fila]