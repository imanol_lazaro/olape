from Modelo.Compras import *
import Controlador.c_productos as c_productos
from Modelo.AccesoDatos.Datos import *


from datetime import datetime

def menu():
    list_menu = ["*** BIENVENIDO AL MENU COMPRAS ***",
    "a) Registrar",
    "b) Listar Rápido",
    "c) Listar Detallado",
    "d) Anular",
    "x) Salir"]
    for a in list_menu:
        print(a)
def ejecutar():
    volver = "si"
    while volver == "si":
        menu()
        opcion = input("Elija una opción: ").lower()
        if opcion == "a":
            registrar()
        elif opcion == "b":
            listar_rapido()
        elif opcion == "c":
            listar_detallado()
        elif opcion == "d":
            anular()
        elif opcion == "x":
            volver_productos = input("Desea volver al Menú General ? : ").lower()
            if volver_productos == "si":
                volver = "no"
        else: 
            print("Ingrese a/b/c/d/x")
def registrar():
    proveedor = input("Proveedor : ").capitalize()
    fecha = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
    obj_compras = Compras(proveedor,fecha)
    volver = "si"
    while volver == "si":
        if len(obj_compras.detalle_compra) > 0:
            volver = input("Desea agregar otro producto ? : ").lower()
        if volver == "si":
            producto = input("Producto : ").capitalize()
            precio = float(input("Precio : "))
            cantidad = int(input("Cantidad : "))
            c_productos.actualizarOCrear(producto, precio, cantidad)
            obj_detallecompra = DetalleCompra(producto,cantidad,precio)
            obj_compras.detalle_compra.append(obj_detallecompra)
    list_compras.append(obj_compras)

def listar_rapido():
    for item, dato in enumerate(list_compras,1):
        if item == 1:
            print("Idem","Proveedor","Fecha y Hora","Total",sep= "   |   ")
        print(item,")","   |   " ,dato.proveedor,"   |   ",dato.fecha,"   |   ", dato.total())
def listar_detallado():
    for item, dato in enumerate(list_compras,1):
        print("Idem","Proveedor","Fecha y Hora",sep="   |    ")
        print(item,")","   |   " ,dato.proveedor,"   |   ",dato.fecha,"   |   ")
        for i,num in enumerate(dato.detalle_compra,1):
            if i == 1 :
                print("Producto","Cantidad","TotalProducto",sep="   |    ")
            print(num.producto,num.cantidad,num.total_detallado(),sep="   |    ")
        print("                TotalGeneral : ",dato.total())
def anular():
    listar_rapido()
    num = int(input("Seleccione la fila que quierre eliminar ?: "))
    num -= 1
    del list_compras[num]
