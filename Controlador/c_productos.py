from Modelo.Productos import Productos
from Modelo.AccesoDatos.Datos import *


def menu():
    list_menu = ["*** BIENVENIDO AL MENU PRODUCTOS ***","a) Registrar","b) Listar","c) Editar","d) Eliminar","x) Salir"]
    for a in list_menu:
        print(a)
def ejecutar():
    volver = "si"
    while volver == "si":
        menu()
        opcion = input("Elija una opción: ").lower()
        if opcion == "a":
            registrar()
        elif opcion == "b":
            listar()
        elif opcion == "c":
            editar()
        elif opcion == "d":
            eliminar()
        elif opcion == "x":
            volver_productos = input("Desea volver al Menú General ? : ").lower()
            if volver_productos == "si":
                volver = "no"
        else: 
            print("Ingrese a/b/c/d/x")
        

def registrar():
    descripcion = input("Descripción: ")
    precio = float(input("Precio: "))
    stock = int(input("stock: "))
    seEncontror = False
    fila = 0
    for dato in list_productos:
        if dato.descripcion.strip().lower() == descripcion.strip().lower():
            seEncontror = True
            break
        fila += 1
    if seEncontror:
        list_productos[fila].descripcion = descripcion 
        list_productos[fila].precio = precio 
        list_productos[fila].stock = stock + list_productos[fila].stock
    else:
        obj_producto = Productos(descripcion, precio, stock)
        list_productos.append(obj_producto)

def actualizarOCrear(producto, precio, cantidad):
    seEncontro = False
    fila = 0
    for dato in list_productos:
        if dato.descripcion.strip().lower() == producto.strip().lower():
            seEncontro = True
            break
        fila += 1

    if seEncontro:
        list_productos[fila].descripcion = producto
        list_productos[fila].precio = precio
        list_productos[fila].stock = cantidad + list_productos[fila].stock
    else:
        obj_producto = Productos(producto, precio, cantidad)
        list_productos.append(obj_producto)



def listar():
    if len(list_productos) == 0:
        list_productos.append(Productos("Manzana", 5, 10))
        list_productos.append(Productos("Platano", 6, 50))
        list_productos.append(Productos("Naranja", 7, 50))

    for item, dato in enumerate(list_productos, 1):
        if item == 1:
            print("Idem","Descripción","Precio S/.","Stock",sep= "   |   ")
        print(item,")","   |   " ,dato.descripcion,"   |   ",dato.precio,"   |   ", dato.stock,)

def editar():
    listar()
    fila = int(input("Elija un Producto: "))
    fila -= 1
    descripcion = input("Nueva Descripción: ")
    precio = float(input("Nuevo Precio: "))
    stock = int(input("Nuevo stock: "))

    list_productos[fila].descripcion = descripcion
    list_productos[fila].precio = precio
    list_productos[fila].stock = stock

def eliminar():
    listar()
    fila = int(input("Elija un Producto: "))
    fila -= 1
    del list_productos[fila]