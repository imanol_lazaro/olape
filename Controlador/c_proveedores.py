from Modelo.Proveedores import *
from Modelo.AccesoDatos.Datos import *

def menu():
    list_menu = ["*** BIENVENUIDO AL MENÚ PROVEEDORES ***",
    "a) Registrar",
    "b) Listar",
    "c) Editar",
    "d) Eliminar",
    "x) Salir"]
    for n in list_menu:
        print(n)
def ejecutar():
    volver = "si"
    while volver == "si":
        menu()
        opcion = input("Elija una opción: ").lower()
        if opcion == "a":
            registrar()
        elif opcion == "b":
            listar()
        elif opcion == "c":
            editar()
        elif opcion == "d":
            eliminar()
        elif opcion == "x":
            volver_productos = input("Desea volver al Menú General ? : ").lower()
            if volver_productos == "si":
                volver = "no"
        else: 
            print("Ingrese a/b/c/d/x")
def registrar():
    nombre = input("Nombre : ").capitalize()
    apellido = input("Apellido : ").capitalize()
    dni = input("Dni : ")
    obj_proveedores = Proveedores(nombre,apellido,dni)
    list_proveedores.append(obj_proveedores)
def listar():
    for item, dato in enumerate(list_proveedores,1):
        if item == 1:
            print("Idem","Nombre","Apellido","Dni",sep= "   |   ")
        print(item,")","   |   " ,dato.nombre,"   |   ",dato.apellido,"   |   ", dato.dni,)
def editar():
    listar()
    num = int(input("Elija el proveedor que quiere editar : "))-1
    nombre = input("Nuevo nombre : ").capitalize()
    apellido = input("Nuevo apellido : ").capitalize()
    dni = input("Nuevo dni : ")
    list_proveedores[num].nombre = nombre
    list_proveedores[num].apellido = apellido
    list_proveedores[num].dni = dni
def eliminar():
    listar()
    num = int(input("Elija el proveedor que quiere eliminar : "))
    num -= 1
    del list_proveedores[num]