from Modelo.Ventas import *
import Controlador.c_productos as c_productos
from Modelo.AccesoDatos.Datos import *
import Controlador.c_clientes as c_clientes


from datetime import datetime



def elegirCliente():
    list_elegircliente = ["*"*24,
    "a) Elijir un cliente",
    "b) Crear cliente"]
    for i in list_elegircliente:
        print(i)

def menu():
    list_menu = ["*** BIENVENIDO AL MENU VENTAS ***",
    "a) Vender",
    "b) Listar Rápido",
    "c) Listar Detallado",
    "d) Anular",
    "x) Salir"]
    for a in list_menu:
        print(a)
def ejecutar():
    volver = "si"
    while volver == "si":
        menu()
        opcion = input("Elija una opción: ").lower()
        if opcion == "a":
            vender()
        elif opcion == "b":
            listar_rapido()
        elif opcion == "c":
            listar_detallado()
        elif opcion == "d":
            anular()
        elif opcion == "x":
            volver_productos = input("Desea volver al Menú General ? : ").lower()
            if volver_productos == "si":
                volver = "no"
        else: 
            print("Ingrese a/b/c/d/x")
def vender():
    CerrarCompra = ""
    while CerrarCompra != "si":
        elegirCliente()
        opcion = input("Ingresa la opción: ").lower()
        if opcion == "a":
            c_clientes.listar()
            a1 = True
            while a1:
                try:
                    fila = int(input("Ingresa el número de la fila: "))
                    fila -= 1
                    cliente = list_clientes[fila]
                    a1 = False
                except ValueError:
                    print("DEBE INTRODUCIR NÚMEROS ENTEROS!!!!")
        elif opcion == "b":
            cliente = c_clientes.registrar()
        fecha = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        obj_ventas = Ventas(cliente,fecha)
        c_productos.listar()
        a2 = True
        while a2:
            try:
                fila = int(input("Ingresa el número de la fila: "))
                fila -= 1
                producto = list_productos[fila]
                a2 = False
            except ValueError:
                print("DEBE INTRODUCIR EL NÚMERO DE FILA EN ENTEROS!!!!")
        a3 = True
        while a3:
            try:
                cantidad = int(input("Ingresa la cantidad: "))
                if cantidad > list_productos[fila].stock:
                    print("La cantidad es mayor que el Stock!!!!!")
                else:
                    a3 = False
            except ValueError:
                print("DEBE INTRODUCIR LA CANTIDAD EN NÚMEROS ENTEROS!!!!")
        codigo = input("Ingresa el código de la venta")
        CerrarCompra = input("Confirmar compra si/no: ").lower()
        if CerrarCompra == "si":
            obj_detalleventas = DetalleVentas(producto,cantidad)
            obj_ventas.detalle_ventas.append(obj_detalleventas)
            list_ventas.append(obj_ventas)
            stock = list_productos[fila].stock - cantidad
            list_productos[fila].stock = stock
    def listar_rapido():
        fecha = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        obj_compras = Compras(proveedor,fecha)
        volver = "si"
        while volver == "si":
            if len(obj_compras.detalle_compra) > 0:
                volver = input("Desea agregar otro producto ? : ").lower()
            if volver == "si":
                producto = input("Producto : ").capitalize()
                precio = float(input("Precio : "))
                cantidad = int(input("Cantidad : "))
                c_productos.actualizarOCrear(producto, precio, cantidad)
                obj_detallecompra = DetalleCompra(producto,cantidad,precio)
                obj_compras.detalle_compra.append(obj_detallecompra)
        list_compras.append(obj_compras)

def listar_rapido():
    for item, dato in enumerate(list_compras,1):
        if item == 1:
            print("Idem","Proveedor","Fecha y Hora","Total",sep= "   |   ")
        print(item,")","   |   " ,dato.proveedor,"   |   ",dato.fecha,"   |   ", dato.total())
def listar_detallado():
    for item, dato in enumerate(list_compras,1):
        print("Idem","Proveedor","Fecha y Hora",sep="   |    ")
        print(item,")","   |   " ,dato.proveedor,"   |   ",dato.fecha,"   |   ")
        for i,num in enumerate(dato.detalle_compra,1):
            if i == 1 :
                print("Producto","Cantidad","TotalProducto",sep="   |    ")
            print(num.producto,num.cantidad,num.total_detallado(),sep="   |    ")
        print("                TotalGeneral : ",dato.total())
def anular():
    listar_rapido()
    num = int(input("Seleccione la fila que quierre eliminar ?: "))
    num -= 1
    del list_compras[num]