class Compras:
    def __init__(self,proveedor,fecha):
        self.proveedor = proveedor
        self.fecha = fecha
        self.detalle_compra = []
    def total(self):
        total = 0
        for tot in self.detalle_compra:
            total += tot.cantidad * tot.precio
        return total

class DetalleCompra:
    def __init__(self,producto,cantidad,precio):
        self.producto = producto
        self.cantidad = cantidad
        self.precio = precio
    def total_detallado(self):
        return self.cantidad*self.precio